module.exports = {
  "onboarding": true,
  "requireConfig": "required",
  "onboardingConfig": {
    "$schema": "https://docs.renovatebot.com/renovate-schema.json",
    "extends": [
      "gitlab>jamietanna/renovate-config-presets"
    ]
  },
}
