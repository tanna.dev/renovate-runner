# Renovate Runner for tanna.dev

An instance of [Renovate On-Prem](https://github.com/mend/renovate-on-prem) running on [Fly.io](https://fly.io).

More details for the how and _why_ can be found [on my blog](https://www.jvt.me/posts/2023/08/30/renovate-fly-io/).

Licensed under Apache-2.0.
